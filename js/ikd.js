$('select').select2({width : '100%'})
$('.hoverme').hover(function(){$(this).find('button').removeClass('hidden')},function(){$(this).find('button').addClass('hidden')})

$(document).on('change','.nilai',function(){
  var elem = $(this);
  var all = elem.parent().parent().find('select');
  var x = 0;
  $.each(all,function(idx,val){
    x += parseInt($(this).val());
  });
  elem.parent().parent().find('.bijine').html('Nilai : '+x);
});

$('.tt').tooltip();

$('form#form-k2').submit(function(e){
  e.preventDefault();
  $('.hasil-k2').removeClass('hidden');
});

$('form#form-k3').submit(function(e){
  e.preventDefault();
  $('.hasil-k3').removeClass('hidden');
});

$('#dp1,#dp2').datetimepicker({
  format : 'DD-MM-YYYY',
  widgetPositioning: {
      horizontal: 'left',
      vertical: 'bottom'
  }
});
